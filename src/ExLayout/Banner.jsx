import React, { Component } from 'react'

export default class Banner
 extends Component {
  render() {
    return (
        <div className="container card" style={{width: '68rem'}}>
  <div className="card-body text-left">
    <h1 className="card-title">A Warm Wellcome!</h1>
    <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
    <a href="#" className="btn btn-primary">Call to Action!</a>
  </div>
</div>
    )
  }
}
