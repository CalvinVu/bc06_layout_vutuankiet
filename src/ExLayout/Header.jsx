import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div className="bg-dark">
        <div className="container d-flex justify-content-between align-items-center p-2" style={{fontSize:"20px"}}>
            <div className="text-white">Start Bootstrap</div>
            <div className="">
                <ul className='d-flex list-unstyled m-auto'>
                    <li><a href="" className='text-blue nav-item active'>Home</a></li>
                    <li><a href="" className='text-white pl-2 nav-item'>About</a></li>
                    <li><a href=""className='text-white px-2 nav-item'>Services</a></li>
                    <li><a href="" className='text-white nav-item'>Contact</a></li>
                </ul>
            </div>
        </div>
      </div>
    )
  }
}
