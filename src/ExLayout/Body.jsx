import React, { Component } from 'react'
import Banner from './Banner'
import Item from './Item'

export default class Body extends Component {
  render() {
    return (
      <div className='m-5'>
        <div className='m-5'><Banner/></div>
        <div className="container d-flex justify-content-around">
                <div className=''><Item/></div>
                <div className=''><Item/></div>
                <div className=''><Item/></div>
                <div className=''><Item/></div>
        </div>
      </div>
    )
  }
}
