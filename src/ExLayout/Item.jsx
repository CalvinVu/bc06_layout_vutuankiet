import React, { Component } from 'react'

export default class Item extends Component {
    img = "http://t0.gstatic.com/licensed-image?q=tbn:ANd9GcQkrjYxSfSHeCEA7hkPy8e2JphDsfFHZVKqx-3t37E4XKr-AT7DML8IwtwY0TnZsUcQ";
  render() {
    return (
       <div className="card" style={{width: '16rem'}}>
  <img src={this.img} className="card-img-top" alt="..." />
  <div className="card-body">
    <h5 className="card-title">Card title</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
  <div className='p-3' style={{background: "#D1B091"}}><a href="#" className="btn btn-primary">Find Out More!</a></div>
</div>
    )
  }
}
