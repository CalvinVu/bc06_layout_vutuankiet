import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className='bg-dark p-3'>
        <div className='text-center text-white'>Copyright © Your Website 2019</div>
      </div>
    )
  }
}
